﻿using System;

namespace SuitSupply.Core.Exceptions
{
    public class ServiceException : Exception
    {       
        public string Message { get; set; }
        public ServiceException( string message)
        {            
            this.Message = message;
        }
    }         
}