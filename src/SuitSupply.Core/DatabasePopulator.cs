﻿using SuitSupply.Core.Entities;
using SuitSupply.Core.Interfaces;
using System.Threading.Tasks;

namespace SuitSupply.Core
{
    public static class DatabasePopulator
    {
        public static async Task PopulateDatabase(IRepository<Product> repository)
        {
            var products = await repository.ListAsync();
            if (products.Count > 0) return;
            await repository.AddAsync(new Product("CODE1", "iPhone", string.Empty, 800.50));
            await repository.AddAsync(new Product("CODE2", "Apple TV", string.Empty, 790.99));
            await repository.AddAsync(new Product("CODE3", "Dell XPS", string.Empty, 1790.99));
            await repository.AddAsync(new Product("CODE4", "HP Probook", string.Empty, 1490.99));
        }
    }
}
