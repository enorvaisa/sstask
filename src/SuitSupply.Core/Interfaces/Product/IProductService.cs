﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SuitSupply.Core.DTO;
using SuitSupply.Core.DTO.Filters;
using SuitSupply.Core.DTO.Product;

namespace SuitSupply.Core.Interfaces.Product
{
    public interface IProductService
    {
        Task<PagedResultDTO<IEnumerable<ProductDTO>>> FindAsync(PagedFilterDTO filter);        
        Task<ProductDTO> GetAsync(int id);
        Task<ProductDTO> CreateAsync(ProductDTO product);
        Task<ProductDTO> UpdateAsync(ProductDTO product);
        Task DeleteAsync(int id);        
    }
}