﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SuitSupply.Core.SharedKernel;

namespace SuitSupply.Core.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {        
        Task<List<TEntity>> PagedListAsync(Expression<Func<TEntity, bool>> query, int pageIndex, int pageSize);
        Task<int> CountAsync(Expression<Func<TEntity, bool>> query);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> query);
        Task<TEntity> GetByIdAsync(int id);
        Task<List<TEntity>> ListAsync();
        Task<TEntity> AddAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}