﻿using System;

namespace SuitSupply.Core.SharedKernel
{    
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public byte[] RawVersion { get; set; }    
        public DateTime LastUpdated { get; set; }
    }
}