﻿using System;
using System.Linq.Expressions;

namespace SuitSupply.Core.SharedKernel
{
    public abstract class BaseSpecification<T>
    {
        private Func<T, bool> _compiledExpression;

        private Func<T, bool> CompiledExpression => _compiledExpression ?? (_compiledExpression = Expression.Compile());

        public abstract Expression<Func<T, bool>> Expression { get; }

        public bool IsSatisfiedBy(T obj)
        {
            return CompiledExpression(obj);
        }
    }
}