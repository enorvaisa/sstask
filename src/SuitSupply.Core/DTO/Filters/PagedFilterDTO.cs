﻿namespace SuitSupply.Core.DTO.Filters
{
    public class PagedFilterDTO
    {
        public string SearchTerm { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}