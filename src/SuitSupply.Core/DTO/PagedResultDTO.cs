﻿namespace SuitSupply.Core.DTO
{
    public class PagedResultDTO<T> 
    {
        public int TotalCount { get; set; }
        public T Data { get; set; }
    }
}