﻿namespace SuitSupply.Core.DTO.Product
{    
    public class ProductDTO
    {
        public int Id { get; set; }        
        public string Code { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public bool IsConfirmed { get; private set; }
        public double? Price { get; set; }

        public static ProductDTO FromProductDTO(Entities.Product product)
        {
            return new ProductDTO
            {
                Id = product.Id,
                Code = product.Code,
                Name = product.Name,
                Photo = product.Photo,
                IsConfirmed = product.IsConfimed,
                Price = product.Price
            };
        }
    }
}
