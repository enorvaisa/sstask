﻿namespace SuitSupply.Core.DTO.Exceptions
{
    public class ApiExceptionDTO
    {        
        public string Message { get; set; }
    }
}
