﻿using System;
using System.Linq.Expressions;
using SuitSupply.Core.Entities;
using SuitSupply.Core.SharedKernel;

namespace SuitSupply.Core.Specifications
{    
    public class ProductSearchSpecification: BaseSpecification<Product>
    {
        private readonly string _searchTerm;

        public ProductSearchSpecification(string searchTerm)
        {
            _searchTerm = searchTerm;           
        }

        public override Expression<Func<Product, bool>> Expression
        {
            get
            {
                if (string.IsNullOrEmpty(_searchTerm))
                    return product => true;
                
                return product =>
                    product.Name.ToLower().Contains(_searchTerm) || product.Code.ToLower().Contains(_searchTerm);
            }
        }
    }
}