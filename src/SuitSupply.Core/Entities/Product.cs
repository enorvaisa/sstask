﻿using System;
using SuitSupply.Core.Constants;
using SuitSupply.Core.SharedKernel;

namespace SuitSupply.Core.Entities
{
    public sealed class Product : BaseEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public double Price { get; private set; }        
        public bool IsConfimed { get; private set; }

        public Product()
        {

        }
        
        public Product(string code, string name, string photo, double price)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Missing product name");

            if (string.IsNullOrEmpty(code))
                throw new ArgumentException("Missing product code");

            LastUpdated = DateTime.UtcNow;
            Code = code;
            Name = name;
            Photo = string.IsNullOrEmpty(photo)? ProductPicture.DefaultImage : photo;
            SetPrice(price);
        }
        
        public void SetPrice(double price)
        {            
            if (price <= 0)
                throw new ArgumentException("Price must be greater than 0");

            IsConfimed = price > 999;
            Price = price;
        }
    }
}