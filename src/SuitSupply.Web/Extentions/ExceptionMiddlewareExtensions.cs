﻿using Microsoft.AspNetCore.Builder;
using SuitSupply.Web.Middlewares.ExceptionMiddleware;

namespace SuitSupply.Web.Extentions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }
}