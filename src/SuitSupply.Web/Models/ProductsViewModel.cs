﻿using SuitSupply.Core.DTO.Product;
using X.PagedList;

namespace SuitSupply.Web.Models
{
    public class ProductsViewModel
    {
        public StaticPagedList<ProductDTO> Data { get; set; }
        public string SearchTerm { get; set; }
    }
}