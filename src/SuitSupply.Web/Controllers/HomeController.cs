﻿using Microsoft.AspNetCore.Mvc;
using SuitSupply.Core;
using SuitSupply.Core.Entities;
using SuitSupply.Core.Interfaces;

namespace SuitSupply.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository<Product> _repository;

        public HomeController(IRepository<Product> repository)
        {
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Seed()
        {
            DatabasePopulator.PopulateDatabase(_repository);
            return RedirectToAction("Index", "Product");
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
