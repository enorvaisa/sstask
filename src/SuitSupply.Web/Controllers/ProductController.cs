﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuitSupply.Core.DTO.Filters;
using SuitSupply.Core.DTO.Product;
using SuitSupply.Core.Interfaces.Product;
using SuitSupply.Infrastructure.Excel;
using SuitSupply.Infrastructure.File;
using SuitSupply.Web.Models;
using X.PagedList;

namespace SuitSupply.Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly IExcelService _excelService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IImageService _imageService;

        public ProductController(IProductService productService,
            IExcelService excelService,
            IHostingEnvironment hostingEnvironment,
            IImageService imageService)
        {
            _productService = productService;
            _excelService = excelService;
            _hostingEnvironment = hostingEnvironment;
            _imageService = imageService;
        }

        public async Task<IActionResult> Index(int? page, string searchTerm)
        {
            var pageIndex = (page ?? 1) - 1;
            const int pageSize = 10;

            var filter = new PagedFilterDTO
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                SearchTerm = searchTerm
            };

            var pagedProducts = await _productService.FindAsync(filter);

            var products = new StaticPagedList<ProductDTO>(pagedProducts.Data,
                pageIndex + 1,
                pageSize,
                pagedProducts.TotalCount);

            return View(new ProductsViewModel{Data = products, SearchTerm = searchTerm});
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(IFormFile file,
            [Bind("Name,Code,Price,Photo")] ProductViewModel product)
        {
            if (!ModelState.IsValid) return View(product);
            var productDTO = new ProductDTO
            {
                Code = product.Code,
                Name = product.Name,
                Price = product.Price
            };
            if (file != null)
            {
                var fileName = await _imageService.UploadImage(file, _hostingEnvironment.WebRootPath);
                productDTO.Photo = fileName;
            }

            await _productService.CreateAsync(productDTO);
            return RedirectToAction("Index");

        }

        public async Task<IActionResult> Details(int id)
        {
            ProductDTO product = await _productService.GetAsync(id);

            if (product == null)
                return RedirectToAction("Index");

            var model = new ProductViewModel
            {
                Id = product.Id,
                Code = product.Code,
                Name = product.Name,
                PhotoName = product.Photo,
                Price = product.Price ?? 0
            };
            return View(model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _productService.DeleteAsync(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Details(int id, IFormFile file,
            [Bind("Name,Code,Price,Photo")] ProductViewModel product)
        {
            if (!ModelState.IsValid) return View(product);
            var productDTO = new ProductDTO
            {
                Id = id,
                Name = product.Name,
                Code = product.Code,
                Price = product.Price
            };
            if (file != null)
            {
                var fileName = await _imageService.UploadImage(file, _hostingEnvironment.WebRootPath);
                productDTO.Photo = fileName;
            }

            await _productService.UpdateAsync(productDTO);
            return RedirectToAction("Index");

        }

        public async Task<IActionResult> ExportProducts()
        {
            var products = await _productService.FindAsync(new PagedFilterDTO());

            string rootFolder = _hostingEnvironment.WebRootPath;

            string fileName = _excelService.ExportProducts(products.Data.ToList(), rootFolder);
            MemoryStream file = await _excelService.GetExportedProducts($"{rootFolder}\\{fileName}");

            return File(file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }
    }
}