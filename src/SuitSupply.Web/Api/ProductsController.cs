﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using SuitSupply.Core.DTO;
using SuitSupply.Core.DTO.Filters;
using SuitSupply.Core.DTO.Product;
using SuitSupply.Core.Interfaces.Product;
using SuitSupply.Infrastructure.Excel;
using SuitSupply.Web.Filters;

namespace SuitSupply.Web.Api
{
    [Route("api/[controller]")]
    [ApiVersion("1.0")]
    [ValidateModel]
    public class ProductsController : Controller
    {
        private readonly IProductService _productService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IExcelService _excelService;

        public ProductsController(IProductService productService,
            IHostingEnvironment hostingEnvironment,
            IExcelService excelService)
        {
            _productService = productService;
            _hostingEnvironment = hostingEnvironment;
            _excelService = excelService;
        }

        [HttpGet]
        public async Task<IActionResult> ListAsync(string searchTerm, int pageIndex, int pageSize)
        {
            PagedResultDTO<IEnumerable<ProductDTO>> products = await _productService.FindAsync(new PagedFilterDTO
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                SearchTerm = searchTerm
            });
            return Ok(products);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            ProductDTO product = await _productService.GetAsync(id);
            return Ok(product);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] ProductDTO productDTO)
        {
            ProductDTO createProduct = await _productService.CreateAsync(productDTO);
            return Ok(createProduct);
        }

        [HttpPut]
        public async Task<IActionResult> EditAsync([FromBody] ProductDTO productDTO)
        {
            ProductDTO createProduct = await _productService.UpdateAsync(productDTO);
            return Ok(createProduct);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Complete(int id)
        {
            await _productService.DeleteAsync(id);
            return Ok();
        }

        [HttpGet("Export")]
        public async Task<IActionResult> ExcelAsync()
        {
            var products = await _productService.FindAsync(new PagedFilterDTO());

            string rootFolder = _hostingEnvironment.WebRootPath;

            string fileName = _excelService.ExportProducts(products.Data.ToList(), rootFolder);
            MemoryStream file = await _excelService.GetExportedProducts($"{rootFolder}\\{fileName}");

            return File(file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }
    }
}