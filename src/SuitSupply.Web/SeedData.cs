﻿using SuitSupply.Core.Entities;
using SuitSupply.Infrastructure.Data;

namespace SuitSupply.Web
{
    public static class SeedData
    {
        public static void PopulateTestData(AppDbContext dbContext)
        {
            var products = dbContext.Products;
            foreach (var item in products)
            {
                dbContext.Remove(item);
            }
            dbContext.SaveChanges();
            dbContext.Products.Add(new Product("CODE1", "iPhone", string.Empty, 800.50));
            dbContext.Products.Add(new Product("CODE2", "Apple TV", "noImage.png", 790.99));
            dbContext.Products.Add(new Product("CODE3", "Dell XPS", "noImage.png", 1790.99));
            dbContext.Products.Add(new Product("CODE4", "HP Probook", "noImage.png", 1490.99));
            dbContext.SaveChanges();
        }

    }
}
