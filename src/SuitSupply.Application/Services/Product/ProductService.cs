﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SuitSupply.Core.DTO;
using SuitSupply.Core.DTO.Filters;
using SuitSupply.Core.DTO.Product;
using SuitSupply.Core.Exceptions;
using SuitSupply.Core.Interfaces;
using SuitSupply.Core.Interfaces.Product;
using SuitSupply.Core.Specifications;

namespace SuitSupply.Application.Services.Product
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Core.Entities.Product> _productRepository;

        public ProductService(IRepository<Core.Entities.Product> productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<PagedResultDTO<IEnumerable<ProductDTO>>> FindAsync(PagedFilterDTO filter)
        {
            var searchQuery = new ProductSearchSpecification(filter.SearchTerm).Expression;            

            var products = await _productRepository.PagedListAsync(searchQuery, 
                filter.PageIndex,
                filter.PageSize);

            var totalCount = await _productRepository.CountAsync(searchQuery);

            var data = products.Select(ProductDTO.FromProductDTO);

            return new PagedResultDTO<IEnumerable<ProductDTO>>
            {
                Data = data,
                TotalCount = totalCount
            };
        }

        public async Task<ProductDTO> GetAsync(int id)
        {
            var product = await _productRepository.GetByIdAsync(id);
            return ProductDTO.FromProductDTO(product);
        }

        public async Task<ProductDTO> CreateAsync(ProductDTO productDTO)
        {
            var productByCode =
                await _productRepository.FirstOrDefaultAsync(x => x.Code.ToLower().Equals(productDTO.Code.ToLower()));

            if (productByCode != null)
                throw new ServiceException("Product exist");

            double price = 0;
            if (productDTO.Price != null)
                price = (double)productDTO.Price;
            
            var product =
                new Core.Entities.Product(productDTO.Code, productDTO.Name, productDTO.Photo, price);

            product = await _productRepository.AddAsync(product);

            productDTO.Id = product.Id;

            return productDTO;
        }

        public async Task DeleteAsync(int id)
        {
            var product = await _productRepository.GetByIdAsync(id);

            if (product == null)
                throw new ServiceException("Product does't exist");

            await _productRepository.DeleteAsync(product);
        }

        public async Task<ProductDTO> UpdateAsync(ProductDTO productDTO)
        {
            var product = await _productRepository.GetByIdAsync(productDTO.Id);

            if (product == null)
                throw new ServiceException("Product does't exist");

            if (!string.IsNullOrEmpty(productDTO.Name))
                product.Name = productDTO.Name;
            if (!string.IsNullOrEmpty(productDTO.Code))
                product.Code = productDTO.Code;
            if (!string.IsNullOrEmpty(productDTO.Photo))
                product.Photo = productDTO.Photo;
            
            product.LastUpdated = DateTime.UtcNow;
                        
            if (productDTO.Price != null)
            {
                product.SetPrice((double)productDTO.Price);
            }                
            
            product = await _productRepository.UpdateAsync(product);

            return ProductDTO.FromProductDTO(product);
        }                
    }
}