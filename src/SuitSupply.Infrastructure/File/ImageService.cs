﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace SuitSupply.Infrastructure.File
{
    public class ImageService : IImageService
    {
        public async Task<string> UploadImage(IFormFile file, string outputLocation)
        {
            if (!IsValidContentType(file.ContentType))
                throw new ArgumentException("Wrong file type");

            var uploads = Path.Combine(outputLocation, "uploads");

            if (file.Length > 0)
            {
                using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }
            return file.FileName;
        }

        private bool IsValidContentType(string ContentType)
        {
            if (ContentType.Equals("image/png"))
                return true;
            if (ContentType.Equals("image/jpeg"))
                return true;
            return false;
        }
    }
}
