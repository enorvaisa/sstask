﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace SuitSupply.Infrastructure.File
{
    public interface IImageService
    {
        Task<string> UploadImage(IFormFile file, string outputLocation);
    }
}
