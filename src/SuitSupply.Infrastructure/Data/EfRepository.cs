using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SuitSupply.Core.Interfaces;
using SuitSupply.Core.SharedKernel;

namespace SuitSupply.Infrastructure.Data
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly AppDbContext _dbContext;

        public EfRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<T>> PagedListAsync(Expression<Func<T, bool>> query, int pageIndex, int pageSize)
        {
            IQueryable<T> q = _dbContext.Set<T>();

            pageIndex = pageIndex < 0 ? 0 : pageIndex;            

            q = q.Where(query).OrderByDescending(x => x.LastUpdated);

            if (pageSize > 0)
                q = q.Skip(pageIndex * pageSize).Take(pageSize);

            return await q.ToListAsync();
        }

        public async Task<int> CountAsync(Expression<Func<T, bool>> query)
        {                                    
            return await _dbContext.Set<T>().Where(query).CountAsync();            
        }
        
        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> query)
        {
            return _dbContext.Set<T>().Where(query).FirstOrDefaultAsync();
        }

        public Task<T> GetByIdAsync(int id)
        {
            return _dbContext.Set<T>().SingleOrDefaultAsync(e => e.Id == id);
        }

        public Task<List<T>> ListAsync()
        {
            return _dbContext.Set<T>().ToListAsync();
        }

        public async Task<T> AddAsync(T entity)
        {
            await _dbContext.Set<T>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public Task DeleteAsync(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            return _dbContext.SaveChangesAsync();
        }

        public async Task<T> UpdateAsync(T entity)
        {           
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
            return entity;
        }
    }
}