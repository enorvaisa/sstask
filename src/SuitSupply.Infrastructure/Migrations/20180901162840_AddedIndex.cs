﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuitSupply.Infrastructure.Migrations
{
    public partial class AddedIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Products_LastUpdated",
                table: "Products",
                column: "LastUpdated");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Products_LastUpdated",
                table: "Products");
        }
    }
}
