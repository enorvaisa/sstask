﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuitSupply.Infrastructure.Migrations
{
    public partial class RenamedTimeStamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Timestamp",
                table: "Products",
                newName: "RawVersion");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RawVersion",
                table: "Products",
                newName: "Timestamp");
        }
    }
}
