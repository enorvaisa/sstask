﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuitSupply.Infrastructure.Migrations
{
    public partial class IsConfirmedColAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsDone",
                table: "Products",
                newName: "IsConfimed");

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "Products",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "IsConfimed",
                table: "Products",
                newName: "IsDone");
        }
    }
}
