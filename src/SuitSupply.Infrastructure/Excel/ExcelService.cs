﻿using OfficeOpenXml;
using SuitSupply.Core.DTO.Product;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SuitSupply.Infrastructure.Excel
{
    public class ExcelService : IExcelService
    {
        public string ExportProducts(List<ProductDTO> products, string outputLocation)
        {
            string fileName = $"Products-{DateTime.UtcNow.Millisecond}.xlsx";
            string fileOutputLocation = Path.Combine(outputLocation, fileName);
            
            FileInfo file = new FileInfo(fileOutputLocation);

            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Product");

                worksheet.Cells[1, 1].Value = "Id";
                worksheet.Cells[1, 2].Value = "Code";
                worksheet.Cells[1, 3].Value = "Name";
                worksheet.Cells[1, 4].Value = "Photo";
                worksheet.Cells[1, 5].Value = "Price";

                int i = 0;
                for (int row = 2; row <= products.Count + 1; row++)
                {
                    worksheet.Cells[row, 1].Value = products[i].Id;
                    worksheet.Cells[row, 2].Value = products[i].Code;
                    worksheet.Cells[row, 3].Value = products[i].Name;
                    worksheet.Cells[row, 4].Value = products[i].Photo;
                    worksheet.Cells[row, 5].Value = products[i].Price;
                    i++;
                }

                package.Save();
            }

            return fileName;
        }

        public async Task<MemoryStream> GetExportedProducts(string fileLocation)
        {
            var memoryStream = new MemoryStream();

            using (var stream = new FileStream(fileLocation, FileMode.Open))
            {
                await stream.CopyToAsync(memoryStream);
            }

            memoryStream.Position = 0;
            
            return memoryStream;
        }
    }
}