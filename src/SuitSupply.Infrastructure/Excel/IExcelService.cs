﻿using SuitSupply.Core.DTO.Product;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SuitSupply.Infrastructure.Excel
{
    public interface IExcelService
    {
        string ExportProducts(List<ProductDTO> products, string outputLocation);
        Task<MemoryStream> GetExportedProducts(string outputLocation);
    }
}
