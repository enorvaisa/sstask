﻿using SuitSupply.Core.Constants;
using Xunit;
using Product = SuitSupply.Core.Entities.Product;

namespace CleanArchitecture.Tests.Core.Entities
{
    public class CreateProduct
    {
        private const string MockProductName = "SomeName";
        private const string MockProductCode = "SomeCode";
        private const string MockProductPhoto = "image.png";
        private const int MockProductprice = 100;

        [Fact]
        public void CreateNewProduct()
        {
            Product product = new Product(MockProductCode, MockProductName, MockProductPhoto, MockProductprice);
            Assert.Equal(MockProductName, product.Name);
            Assert.Equal(MockProductCode, product.Code);
            Assert.Equal(MockProductPhoto, product.Photo);
            Assert.Equal(MockProductprice, product.Price);
        }

        [Fact]
        public void CreateNewProductWithNoName()
        {
            var ex = Record.Exception(() =>
            {
                new Product(MockProductCode, string.Empty, MockProductPhoto, MockProductprice);
            });
            Assert.NotNull(ex);
        }

        [Fact]
        public void CreateNewProductWithNoCode()
        {
            var ex = Record.Exception(() =>
            {
                new Product(string.Empty, MockProductName, MockProductPhoto, MockProductprice);
            });
            Assert.NotNull(ex);
        }

        [Fact]
        public void CreateNewProductWithNoPhoto()
        {
            var product = new Product(MockProductCode, MockProductName, string.Empty, MockProductprice);
            Assert.Equal(product.Photo, ProductPicture.DefaultImage);
        }

        [Fact]
        public void CreateNewProductWith0Price()
        {
            var ex = Record.Exception(() => { new Product(MockProductCode, MockProductName, MockProductPhoto, 0); });
            Assert.NotNull(ex);
        }
        
        [Fact]
        public void CreateNewConfirmedProduct()
        {
            Product product = new Product(MockProductCode, MockProductName, MockProductPhoto, 1000);
            Assert.True(product.IsConfimed);
        }
    }
}