﻿using Moq;
using SuitSupply.Application.Services.Product;
using SuitSupply.Core.DTO.Product;
using SuitSupply.Core.Entities;
using SuitSupply.Core.Interfaces;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace SuitSupply.Tests.Service
{
    public class ProductServiceShould
    {

        [Fact]
        public async Task CreateProduct()
        {
            var repositoryMock = new Mock<IRepository<Product>>();
            repositoryMock.Setup(x => x.FirstOrDefaultAsync(It.IsAny<Expression<Func<Product, bool>>>())).ReturnsAsync(() => null);
            repositoryMock.Setup(x => x.AddAsync(It.IsAny<Product>())).ReturnsAsync(() => new Product
            {
                Id = 1,
                Code = "SomeCODE",
                Name = "SomeNAME",
                Photo = "Somephoto.png",                
            });

            var productDTO = new ProductDTO
            {
                Code = "SomeCODE",
                Name = "SomeNAME",
                Photo = "Somephoto.png",
                Price = 100
            };

            var service = new ProductService(repositoryMock.Object);

            var result = await service.CreateAsync(productDTO);

            repositoryMock.Verify(x => x.FirstOrDefaultAsync(It.IsAny<Expression<Func<Product, bool>>>()), Times.Once);
            repositoryMock.Verify(x => x.AddAsync(It.IsAny<Product>()), Times.Once);

            Assert.Equal(productDTO.Code, result.Code);
            Assert.Equal(productDTO.Name, result.Name);
            Assert.Equal(productDTO.Photo, result.Photo);
            Assert.Equal(productDTO.Price, result.Price);
        }

        //...
    }
}
