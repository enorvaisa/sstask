﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SuitSupply.Core.Entities;
using SuitSupply.Infrastructure.Data;
using Xunit;

namespace CleanArchitecture.Tests.Integration.Data
{
    public class EfRepositoryShould
    {
        private AppDbContext _dbContext;

        private static DbContextOptions<AppDbContext> CreateNewContextOptions()
        {
            // Create a fresh service provider, and therefore a fresh
            // InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            builder.UseInMemoryDatabase("suitsupply")
                .UseInternalServiceProvider(serviceProvider);

            return builder.Options;
        }

        [Fact]
        public async Task AddItem()
        {
            var repository = GetRepository();
            var item = new ProductBuilder().Build();

            await repository.AddAsync(item);

            var newItem = await repository.ListAsync();
            var firstItem = newItem.FirstOrDefault();

            Assert.Equal(item, firstItem);
            Assert.True(firstItem?.Id > 0);
        }

        [Fact]
        public async Task GetById()
        {
            var repository = GetRepository();
            var item = new ProductBuilder().Build();

            var product = await repository.AddAsync(item);

            var returnedProduct = await repository.GetByIdAsync(product.Id);
            
            Assert.Equal(item, returnedProduct);
        }              
        
        [Fact]
        public async Task FirstOrDefault()
        {
            var repository = GetRepository();
            var item = new ProductBuilder().Build();

            var product = await repository.AddAsync(item);

            var returnedProduct = await repository.FirstOrDefaultAsync(x => x.Id == product.Id);
            
            Assert.Equal(item, returnedProduct);
        }
        
        [Fact]
        public async Task CountItems()
        {
            var repository = GetRepository();
            var item = new ProductBuilder().Build();

            var product = await repository.AddAsync(item);

            var count = await repository.CountAsync(x => x.Id == product.Id);
            
            Assert.Equal(1,count);
        }

        [Fact]
        public async Task UpdateItemAfterAddingIt()
        {
            // add an item
            var repository = GetRepository();
            var code = Guid.NewGuid().ToString();
            var item = new ProductBuilder().Code(code).Build();

            await repository.AddAsync(item);

            // detach the item so we get a different instance
            _dbContext.Entry(item).State = EntityState.Detached;

            // fetch the item and update its title
            var newItem = await repository.ListAsync();
            var firstItem = newItem.FirstOrDefault(i => i.Code == code);
            Assert.NotNull(newItem);
            Assert.NotSame(item, newItem);
            var newCode = Guid.NewGuid().ToString();
            firstItem.Code = newCode;

            // Update the item
            await repository.UpdateAsync(firstItem);
            var updatedItem = await repository.ListAsync();
            var firstUpdatedItem = updatedItem.FirstOrDefault(i => i.Code == newCode);

            Assert.NotNull(firstUpdatedItem);
            Assert.NotEqual(item.Code, firstUpdatedItem.Code);
            Assert.Equal(firstItem.Id, firstUpdatedItem.Id);
        }

        [Fact]
        public async Task DeleteItemAfterAddingIt()
        {
            // add an item
            var repository = GetRepository();
            var code = Guid.NewGuid().ToString();
            var item = new ProductBuilder().Code(code).Build();
            await repository.AddAsync(item);

            // delete the item
            await repository.DeleteAsync(item);

            // verify it's no longer there
            Assert.DoesNotContain(await repository.ListAsync(),
                i => i.Code == code);
        }

        private EfRepository<Product> GetRepository()
        {
            var options = CreateNewContextOptions();            

            _dbContext = new AppDbContext(options);
            return new EfRepository<Product>(_dbContext);
        }
    }
}