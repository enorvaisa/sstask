﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SuitSupply.Core.DTO;
using SuitSupply.Core.DTO.Product;
using SuitSupply.Web;
using Xunit;

namespace CleanArchitecture.Tests.Integration.Web
{
    public class ApiProductsControllerShould : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public ApiProductsControllerShould(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CreateProduct()
        {
            HttpResponseMessage response;
            var productDTO = new ProductDTO
            {
                Code = "AAA123",
                Name = "TV",
                Photo = "noimage.png",
                Price = 1024
            };

            using (_client)
            {
                var request = JsonConvert.SerializeObject(productDTO);
                response = await _client.PostAsync("/api/Products",
                    new StringContent(request, Encoding.UTF8, "application/json"));
            }

            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ProductDTO>(stringResponse);

            Assert.Equal(productDTO.Code, result.Code);
            Assert.Equal(productDTO.Name, result.Name);
            Assert.Equal(productDTO.Photo, result.Photo);
            Assert.Equal(productDTO.Price, result.Price);
        }
        
        [Fact]
        public async Task UpdateProduct()
        {
            HttpResponseMessage response;
            var productDTO = new ProductDTO
            {
                Id = 1,
                Code = "AAA123",
                Name = "TV",
                Photo = "noimage.png",
                Price = 1024
            };

            using (_client)
            {
                var request = JsonConvert.SerializeObject(productDTO);
                response = await _client.PutAsync("/api/Products",
                    new StringContent(request, Encoding.UTF8, "application/json"));
            }

            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ProductDTO>(stringResponse);

            Assert.Equal(productDTO.Code, result.Code);
            Assert.Equal(productDTO.Name, result.Name);
            Assert.Equal(productDTO.Photo, result.Photo);
            Assert.Equal(productDTO.Price, result.Price);
        }

        [Fact]
        public async Task GetProduct()
        {
            HttpResponseMessage response;        

            using (_client)
            {                
                response = await _client.GetAsync("/api/Products/1");
            }

            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ProductDTO>(stringResponse);

            Assert.NotNull(result);            
        }

        [Fact]
        public async Task DeleteProduct()
        {
            HttpResponseMessage deleteResponse;
            HttpResponseMessage getResponse;

            using (_client)
            {                
                deleteResponse = await _client.DeleteAsync("/api/Products/1");
                getResponse = await _client.GetAsync("/api/Products/1");
            }

            deleteResponse.EnsureSuccessStatusCode();                    
                        
            Assert.Equal(HttpStatusCode.OK, deleteResponse.StatusCode);
            Assert.Equal(HttpStatusCode.InternalServerError, getResponse.StatusCode);
            
        }

        [Fact]
        public async Task GetProducts()
        {
            HttpResponseMessage response;

            using (_client)
            {
                response = await _client.GetAsync("/api/Products");
            }

            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResultDTO<IEnumerable<ProductDTO>>>(stringResponse);

            Assert.NotNull(result);
        }
    }
}