﻿using SuitSupply.Core.Entities;

namespace CleanArchitecture.Tests
{
    public class ProductBuilder
    {
        private readonly Product _product = new Product("SomeCode","SomeName","SomePhoto.png",100);

        public ProductBuilder Code(string title)
        {
            _product.Code = title;
            return this;
        }

        public Product Build() => _product;
    }
}
